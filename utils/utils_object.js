export function isObjectEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0) return false;
    if (obj.length === 0) return true;

    // Check if object is numeric
    if (isObjectNumeric(obj)) return false;

    // If it isn't an object at this point
    // it is empty, but it can't be anything *but* empty
    // Is it empty?  Depends on your application.
    if (typeof obj !== "object") return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

export function isObjectAnArray(object) {

    var objectisarray = false;

    if (object.constructor === Array) {
        objectisarray = true;
    }

    return objectisarray;
}

export function isObjectAFunction(functionToCheck) {
    var getType = {};
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

export function checkObjectGraph(obj, graphPath) {
    
    if (obj) {
        
        let root = obj;
        let graph_not_exits;

        graphPath.split('.').forEach(element => {

            if (!isObjectEmpty(root[element])) {
                root = root[element];
            } else {
                graph_not_exits=false; // bad property
            }
        });

        if(!graph_not_exits){
            return false;
        }else{
            return true;
        }

    }else{
        return false; // bad object
    }
   
}


export function isObjectNumeric(object){
    
    var numeric = false;

    if(typeof(object)==="number"){
        
        numeric=true;
    }
    
    return numeric;
}