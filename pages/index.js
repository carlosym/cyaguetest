import React, { Component } from 'react';
//import Link from 'next/link'
import fetch from 'isomorphic-unfetch'
import "./../styles/index.scss";
import { Games } from './../components/games/games';

/*const IndexPage = (props) => (
    <div className="grid-frame">
        <Games games={props.games} />
    </div>
);*/


class IndexPage extends Component {

    render() {

        return (
            <div className="grid-frame">
                <Games games={this.props.games} />
            </div>
        );
    }
}

IndexPage.getInitialProps = async function () {
    const res = await fetch('https://www.speedrun.com/api/v1/games')
    const data = await res.json();

    return {
        games: data
    }
}

export default IndexPage;