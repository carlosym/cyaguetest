import React, { Component } from 'react';
//import Link from 'next/link'
import fetch from 'isomorphic-unfetch'
import "./../styles/game.scss";
import { Game } from './../components/game/game';

const GamePage = (props) => (
    <div className="grid-frame">
        <Game game={props.data_game} game_run={props.data_game_run} game_player={props.data_game_player}/>}
    </div>
);

GamePage.getInitialProps = async function (context) {
    
    const { id } = context.query;
    let dynamic_url_game_run = 'https://www.speedrun.com/api/v1/runs?game='+id;
    let dynamic_url_game = 'https://www.speedrun.com/api/v1/games/'+id;
    const res_game_run = await fetch(dynamic_url_game_run);
    const res_game = await fetch(dynamic_url_game);
    const data_game_run = await res_game_run.json();
    const data_game = await res_game.json();

    let first_run=false;
    let data_player = false;

    if(data_game_run.data.length > 0){
        
        first_run = data_game_run.data[0]

        if(first_run.players.length>0){

            if(first_run.players[0].uri){
                const res_player = await fetch(first_run.players[0].uri);
                let player_json = await res_player.json();
                data_player=player_json.data;
            }
            
        }
    }

    return {
        data_game: data_game.data,
        data_game_run:first_run,
        data_game_player:data_player
    }
}

export default GamePage;