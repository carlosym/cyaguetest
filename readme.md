# Readme

## About the project

This project has been developed using [Next.js](https://nextjs.org/) , a framework for creating React.js with SSR functionalities.

The project has two pages /pages/index.js and /pages/game.js, these pages are the root components of the project, from there some React components are called. The React components are located in the components folder.

[Next.js](https://nextjs.org/) manages the different routes for performing the pages rendering in the server side. A file called server.js has been created for configure the server and create and clean the url that goes to game. 

The Styles have been developed with [SASS](https://sass-lang.com/) and the layouts have been done using [foundation for apps](https://foundation.zurb.com/apps.html).

The project also has some component tests done with [Jest](https://jestjs.io/).

# Play

## Install dependencies

Go to the project folder and type in the cmd:

```
npm install
```

## Start app in developer mode

```
npm run dev
```

## Build app for production

```
npm run build
```

## Start app production

```
npm run start
```

## Run test

```
npm run test
```