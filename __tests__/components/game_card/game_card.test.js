import * as React from 'react'
import {GameCard} from './../../../components/game_card/game_card';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

describe('game_card', () => {

    let data ={
        game_logo: {uri: "https://www.speedrun.com/themes/fishy/cover-256.png", width: 360, height: 256},
        game_name: "! Fishy !",
        player_name: "f1",
        run_time: "2016-05-24",
        run_video: "https://youtu.be/-Vesbd8uJzE"
    };

    it('Render component snapshot', function () {
        const component = renderer.create(<GameCard data={data}/>);
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('Render component tittle', function () {
        let wrapper = shallow(<GameCard data={data}/>);
        expect(wrapper.find('h4').text()).toBe(data.game_name);
    });  

    it('Render component game name', function () {
        let wrapper = shallow(<GameCard data={data}/>);
        expect(wrapper.find('.game_name_two').text()).toBe("Game: "+data.game_name);
    }); 
    
    it('Render component game player', function () {
        let wrapper = shallow(<GameCard data={data}/>);
        expect(wrapper.find('.game_player').text()).toBe("Player: "+data.player_name);
    });
    
    it('Render component run time', function () {
        let wrapper = shallow(<GameCard data={data}/>);
        expect(wrapper.find('.game_run_time').text()).toBe("time: "+data.run_time);
    });  

    it('Render component link video', function () {
        let wrapper = shallow(<GameCard data={data}/>);
        let link = "<a class=\"game_video\" href=\"https://youtu.be/-Vesbd8uJzE\" target=\"_blank\">See the video</a>"
        expect(wrapper.find('.game_video').html()).toBe(link);
    });  

    it('Render component game image', function () {
        let wrapper = shallow(<GameCard data={data}/>);
        let image = "<img class=\"image\" src=\"https://www.speedrun.com/themes/fishy/cover-256.png\" height=\"256\" width=\"360\"/>";
        expect(wrapper.find('.image').html()).toBe(image);
    });


});
