import * as React from 'react'
import {ListItem} from './../../../../components/list/list_item/list_item';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

describe('list_item', () => {

    //defining some props
    let text = "007 Racing";
    let logo = {uri: "https://www.speedrun.com/themes/007_Legends/cover-128.png", width: 128, height: 148};
    let id = "o1y93e46";

    it('Render component snapshot', function () {
        const component = renderer.create(<ListItem text={text} logo={logo} id={id}/>);
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('Render component text', function () {
        let wrapper = shallow(<ListItem text={text} logo={logo} id={id}/>);
        expect(wrapper.find('.text').text()).toBe(text);
    });

    it('Render component Image', function () {
        let wrapper = shallow(<ListItem text={text} logo={logo} id={id}/>);
        let image = "<img id=\"image-o1y93e46\" src=\"https://www.speedrun.com/themes/007_Legends/cover-128.png\" height=\"50px\" width=\"100px\"/>";
        expect(wrapper.find('#image-o1y93e46').html()).toBe(image);
    });    
});
