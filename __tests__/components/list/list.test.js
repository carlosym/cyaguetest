import * as React from 'react'
import {List} from './../../../components/list/list';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

describe('list_item', () => {

    //defining some props
    let items = [
        {
            text:"Item1",
            logo:{uri: "https://www.speedrun.com/themes/007_Legends/cover-128.png", width: 128, height: 148},
            id:"item1"
        },
        {
            text:"Item2",
            logo:{uri: "https://www.speedrun.com/themes/007_Legends/cover-128.png", width: 128, height: 148},
            id:"item1"
        },
        {
            text:"Item3",
            logo:{uri: "https://www.speedrun.com/themes/007_Legends/cover-128.png", width: 128, height: 148},
            id:"item1"
        }
    ];

    let tittle="This is my list";

    it('Render component snapshot', function () {
        const component = renderer.create(<List items={items} tittle={tittle}/>);
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('Check num items', function () {
        const component = renderer.create(<List items={items} tittle={tittle}/>);
        const tree = component.toJSON();
        let items_from_tree = tree.children[1].children;
        expect(items_from_tree).toHaveLength(3);
    });   
});