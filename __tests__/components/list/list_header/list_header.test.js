import * as React from 'react'
import {ListHeader} from './../../../../components/list/list_header/list_header';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

describe('list_header', () => {

    //defining some props
    let tittle = "World records speedruns";
    let num_items = 20;

    it('Render component snapshot', function () {
        const component = renderer.create(<ListHeader tittle={tittle} num_items={num_items}/>);
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('Render component text', function () {
        let wrapper = shallow(<ListHeader tittle={tittle} num_items={num_items}/>);
        expect(wrapper.find('h4').text()).toBe("World records speedruns (20)");
    });
});

