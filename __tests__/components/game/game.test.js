import * as React from 'react'
import {Game} from './../../../components/game/game';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import gameJSON from './game.json';

describe('game', () => {

    it('Render component snapshot', function () {
        const component = renderer.create(<Game game={gameJSON.data_game} run={gameJSON.data_game_run} player={gameJSON.data_game_player}/>);
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});