import * as React from 'react'
import {Games} from './../../../components/games/games';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import gamesJSON from './games.json';

describe('games', () => {

    let games_array=gamesJSON;

    it('Render component snapshot', function () {
        const component = renderer.create(<Games games={games_array}/>);
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});