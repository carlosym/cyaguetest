import * as React from 'react'
import {PageHeader} from './../../../components/page_header/page_header';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

describe('page_header', () => {

    //defining some props
    let tittle="21Buttons";
    let author="Author";

    it('Render component snapshot', function () {
        const component = renderer.create(<PageHeader tittle={tittle} author={author}/>);
        const tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('Render tittle', function () {
        let wrapper = shallow(<PageHeader tittle={tittle} author={author}/>);
        expect(wrapper.find('h4').text()).toBe(tittle);
    });   

    it('Render author', function () {
        let wrapper = shallow(<PageHeader tittle={tittle} author={author}/>);
        expect(wrapper.find('.author').text()).toBe("by "+author);
    });   
});