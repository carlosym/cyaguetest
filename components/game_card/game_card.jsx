import React, { Component } from 'react';

export class GameCard extends Component {

    render() {

        let data = this.props.data;

        return (
            <div className="game-card-component grid-block vertical">
                <div className="grid-block shrink align-center game_name"><h4>{data.game_name}</h4></div>
                <div className="grid-block shrink align-center image-layout">
                    <img className="image" src={data.game_logo.uri} height={data.game_logo.height} width={data.game_logo.width}></img>
                </div>
                <div className="grid-block shrink align-center card-item game_name_two" >Game: {data.game_name}</div>
                <div className="grid-block shrink align-center card-item game_player">Player: {data.player_name}</div>
                <div className="grid-block shrink align-center card-item game_run_time">time: {data.run_time}</div>
                <div className="grid-block shrink align-center card-item "><a className="game_video" href={data.run_video} target="_blank">See the video</a></div>
            </div>
        );
    }
}