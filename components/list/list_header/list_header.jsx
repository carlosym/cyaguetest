import React, { Component } from 'react';
//import './list_header.scss';

export class ListHeader extends Component {

    render() {

        let tittle = this.props.tittle;
        let num_items = this.props.num_items;

        return (
            <div className="list-header-component">
                <h4>{tittle} ({num_items})</h4>
            </div>
        );
    }
}