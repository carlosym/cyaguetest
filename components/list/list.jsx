import React, { Component } from 'react';
//import components
import {ListHeader} from './list_header/list_header';
import {ListItem} from './list_item/list_item';

export class List extends Component {

    renderItems(items){

        return (
            items.map((item, index) => {
                return(<ListItem 
                    text={item.text} 
                    logo={item.logo} 
                    id={item.id} 
                    key={index} />) 
            })
        )
    }

    render() {

        let items =  this.props.items;
        let tittle = this.props.tittle;

        return (
            <div className="grid-block vertical list-component ">
                <ListHeader tittle={tittle} num_items={items.length}/>
                <div className="grid-block vertical list">
                    {this.renderItems(items)}            
                </div>
            </div>
        );
    }
}