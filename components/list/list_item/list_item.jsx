import React, { Component } from 'react';
import Link from 'next/link'

export class ListItem extends Component {

    render() {

        let text = this.props.text;
        let logo = this.props.logo;
        let id = this.props.id;

        return (
            <Link as={`/game/${id}`} href={`/game?id=${id}`}>
                <a className="list-item-component grid-block shrink">
                    <div className="grid-block shrink image-div">
                        <img id={`image-${id}`} src={logo.uri} height="50px" width="100px"></img>
                    </div>
                    <div className="grid-block text">{text}</div>
                </a>
            </Link>
        );
    }
}