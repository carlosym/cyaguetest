import React, { Component } from 'react';

export class PageHeader extends Component {

    render() {

        let tittle = this.props.tittle;
        let author = this.props.author;

        return (
            <div className="header-component grid-block shrink">
                <div className="grid-block vertical">
                    <h4>{tittle}</h4>
                    <div className="grid-block author shrink">by {author}</div>
                </div>
            </div>
        );
    }
}