import React, { Component } from 'react';
//import components
import { PageHeader } from './../page_header/page_header';
import { List } from './../list/list';
//import helper
import { getGamesListFromJSONArray } from './helper';

export class Games extends Component {

    render() {

        let games = getGamesListFromJSONArray(this.props.games.data);

        return (
            <div className="games-component grid-block vertical">
                <PageHeader
                    tittle="21Buttons test"
                    author="Carlos Yagüe Méndez"
                    source_code="url bitbucket" />
                <div className="grid-block">
                    <div className="grid-block"></div>
                    <div className="grid-block"><List
                    items={games}
                    tittle="World records speedruns" /></div>
                    <div className="grid-block"></div>
                </div>
            </div>
        );
    }
}