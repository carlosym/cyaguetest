export function getGamesListFromJSONArray(json_array){

    let list = [];

    json_array.forEach(element => {
        
        let listItem ={};
        listItem["text"]=element.names.international;
        listItem["logo"]=element.assets["cover-medium"];
        listItem["id"]=element.id;
        list.push(listItem);
    });

    return list;
}