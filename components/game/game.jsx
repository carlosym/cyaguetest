import React, { Component } from 'react';
//import components
import {PageHeader} from './../page_header/page_header';
import { GameCard } from '../game_card/game_card';
//import helper
import {getGameCardDataFromSources} from './helper';

export class Game extends Component {

    render() {

        let game = this.props.game;
        let run = this.props.game_run;
        let player = this.props.game_player;
        let data_game_card = getGameCardDataFromSources(game,run,player);

        return (
            <div className="game-component grid-block vertical">
                <PageHeader
                    tittle="21Buttons test"
                    author="Carlos Yagüe Méndez"
                    source_code="url bitbucket" />
                <div className="grid-block">
                    <div className="grid-block"></div>
                    <div className="grid-block shrink"><GameCard data={data_game_card}/></div>
                    <div className="grid-block"></div>
                </div>
            </div>
        );
    }
}