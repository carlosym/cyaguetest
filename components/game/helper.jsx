

export function getGameCardDataFromSources(game, run, player) {

    let data={};
    data["player_name"]="?";
    data["run_video"]="?";
    data["run_time"]="?";
    data["game_logo"]="?";
    data["game_name"]="?";

    if(run){

        data.run_time=run.date;
        if(run.videos.links.length>0){
            data["run_video"]=run.videos.links[0].uri;
        }
    }

    if(player){

        if(player.names){
            if(player.names.international){
                data.player_name=player.names.international;
            }
        }
    }

    if(game){
        if(game.assets){
            if(game.assets["cover-large"]){
                data.game_logo=game.assets["cover-large"];
            }
        }

        if(game.names){
            if(game.names.international){
                data.game_name=game.names.international;
            }
        }
    }

    return data;
}